package ru.volnenko.se.listener.project;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volnenko.se.event.ConsoleEvent;
import ru.volnenko.se.listener.AbstractListener;

/**
 * @author Denis Volnenko
 */
@Component
public final class ProjectRemoveListener extends AbstractListener {

    @Override
    public String command() {
        return "project-remove";
    }

    @Override
    public String description() {
        return "Remove selected project.";
    }

    @Override
    @EventListener(condition = "@projectRemoveListener.command() == #event.name")
    public void handler(final ConsoleEvent event) {

    }

}
