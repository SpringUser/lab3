package ru.volnenko.se.listener.data.json;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volnenko.se.event.ConsoleEvent;
import ru.volnenko.se.listener.AbstractListener;
import ru.volnenko.se.constant.DataConstant;

import java.io.File;
import java.nio.file.Files;

/**
 * @author Denis Volnenko
 */
@Component
public final class DataJsonClearListener extends AbstractListener {

    @Override
    public String command() {
        return "data-json-clear";
    }

    @Override
    public String description() {
        return "Remove JSON file.";
    }

    @Override
    @EventListener(condition = "@dataJsonClearListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        final File file = new File(DataConstant.FILE_JSON);
        Files.deleteIfExists(file.toPath());
    }

}
