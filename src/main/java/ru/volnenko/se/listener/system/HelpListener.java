package ru.volnenko.se.listener.system;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volnenko.se.event.ConsoleEvent;
import ru.volnenko.se.listener.AbstractListener;

/**
 * @author Denis Volnenko
 */
@Component
public final class HelpListener extends AbstractListener {

    @Override
    public String command() {
        return "help";
    }

    @Override
    public String description() {
        return "Show all commands.";
    }

    @Override
    @EventListener(condition = "@helpListener.command() == #event.name")
    public void handler(final ConsoleEvent event) {
        for (AbstractListener command: bootstrap.getListCommand()) {
            System.out.println(command.command()+ ": " + command.description());
        }
    }

}
