package ru.volnenko.se.listener.task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.volnenko.se.api.service.ITaskService;
import ru.volnenko.se.event.ConsoleEvent;
import ru.volnenko.se.listener.AbstractListener;
import ru.volnenko.se.service.TaskService;

/**
 * @author Denis Volnenko
 */
@Component
public final class TaskCreateListener extends AbstractListener {

    @Autowired
    private ITaskService taskService;

    @Override
    public String command() {
        return "task-create";
    }

    @Override
    public String description() {
        return "Create new task.";
    }

    @Override
    @EventListener(condition = "@taskCreateListener.command() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        final String name = bootstrap.nextLine();
        taskService.createTask(name);
        System.out.println("[OK]");
        System.out.println();
    }

}
