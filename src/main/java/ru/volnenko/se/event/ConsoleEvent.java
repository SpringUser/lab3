package ru.volnenko.se.event;

public class ConsoleEvent {

    private String name;

    public ConsoleEvent() {

    }

    public ConsoleEvent(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
