package ru.volnenko.se.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.volnenko.se.api.service.IProjectService;
import ru.volnenko.se.api.service.ITaskService;
import ru.volnenko.se.entity.Project;
import ru.volnenko.se.entity.Task;
import ru.volnenko.se.repository.TaskRepository;

import java.util.Collection;
import java.util.List;

/**
 * @author Denis Volnenko
 */
@Service
public class TaskService implements ITaskService {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private IProjectService projectService;

    @Transactional
    public Task createTask(final String name) {
        if (name == null || name.isEmpty()) return null;
        Task t = new Task();
        t.setName(name);
        return taskRepository.save(t);
    }

    
    public Task getTaskById(final String id) {
        return taskRepository.findById(id).orElse(null);
    }

    
    @Transactional
    public Task merge(final Task task) {
        return taskRepository.save(task);
    }

    @Transactional
    public void removeTaskById(final String id) {
        taskRepository.deleteById(id);
    }

    
    public List<Task> getListTask() {
        return taskRepository.findAll();
    }

    @Transactional
    public void clear() {
        taskRepository.deleteAll();
    }

    @Transactional
    public Task createTaskByProject(final String projectId, final String taskName) {
        final Project project = projectService.getProjectById(projectId);
        if (project == null) return null;
        Task t = taskRepository.findFirstByName(taskName).orElse(new Task());
        if(t.getName().isEmpty()) t.setName(taskName);
        t.setProjectId(project.getId());
        return t;
    }

    
    public Task getByOrderIndex(Integer orderIndex) {
        return null;
        //return taskRepository.getByOrderIndex(orderIndex);
    }

    @Transactional
    public void merge(Task... tasks) {
        for(Task t:tasks){
            taskRepository.save(t);
        }
    }

    
    public void load(Task... tasks) {
        taskRepository.deleteAll();
        for(Task t:tasks){
            taskRepository.save(t);
        }
    }

    
    public void load(Collection<Task> tasks) {
        taskRepository.deleteAll();
        taskRepository.saveAll(tasks);
    }

    
    public void removeTaskByOrderIndex(Integer orderIndex) {
        //taskRepository.removeTaskByOrderIndex(orderIndex);
    }

}
