package ru.volnenko.se.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.volnenko.se.api.service.IProjectService;
import ru.volnenko.se.entity.Project;
import ru.volnenko.se.repository.ProjectRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * @author Denis Volnenko
 */
@Service
public class ProjectService implements IProjectService {

    @Autowired
    private ProjectRepository projectRepository;

    @Transactional
    public Project createProject(final String name) {
        if (name == null || name.isEmpty()) return null;
        Project p = new Project();
        p.setName(name);
        projectRepository.save(p);
        return p;
    }

    @Transactional
    public Project merge(final Project project) {
        projectRepository.save(project);
        return project;
    }

    public Project getProjectById(final String id) {
        Optional<Project> p = projectRepository.findById(id);
        return p.orElse(null);
    }

    @Transactional
    public void removeProjectById(final String id) {
        projectRepository.deleteById(id);
    }

    public List<Project> getListProject() {
        return projectRepository.findAll();
    }

    @Transactional
    public void clear() {
        projectRepository.deleteAll();
    }

    @Transactional
    public void merge(Project... projects) {
        if (projects == null) return;
        for (final Project project : projects) {
            projectRepository.save(project);
        }

    }

    @Transactional
    public void load(Collection<Project> projects) {
        projectRepository.deleteAll();
        projectRepository.saveAll(projects);
    }

    @Transactional
    public void load(Project... projects) {
        if (projects == null) return;
        projectRepository.deleteAll();
        for (final Project project : projects) {
            projectRepository.save(project);
        }
    }

    public Project removeByOrderIndex(Integer orderIndex) {
        return null;
    }

}
